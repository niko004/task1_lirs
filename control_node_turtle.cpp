#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <termios.h>
//#include <unistd.h>

ros::Publisher controller_pub;

int getHit()
{
    static struct termios oldt, newt;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    int ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return ch;
}

void sendCommand(float linear_vel, float angular_vel)
{
    geometry_msgs::Twist cmd_vel;
    cmd_vel.linear.x = linear_vel;
    cmd_vel.angular.z = angular_vel;
    controller_pub.publish(cmd_vel);
}

void handleHits()
{
    float linear_vel = 0;
    float angular_vel = 0;
    char key;
    while(ros::ok)
    {
        linear_vel = 0;
        angular_vel = 0;
        key = getHit();
        switch (key)
        {
            case 'w':
                linear_vel = 1;
                break;
            case 's':
                linear_vel = -1;
                break;
            case 'a':
                angular_vel = 1;
                break;
            case 'd':
                angular_vel = -1;
                break;
        }

        sendCommand(linear_vel, angular_vel);
    }
}

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "control_node");
    ros::NodeHandle nodeHandle;
    controller_pub = nodeHandle.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 100);
    ros::Rate loop_rate(100);

    handleHits();

    return 0;
}
